# Information / Информация

SPEC-файл для создания RPM-пакета **exim**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/exim`.
2. Установить пакет: `dnf install exim`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)